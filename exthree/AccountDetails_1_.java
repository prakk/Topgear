package exthree;

public class AccountDetails {
    static int    accNumber;
    static String accName;
    static float accBalance;
    static long creNumber=System.currentTimeMillis();
    public static void acctNo ( int accNumber) {
        AccountDetails.accNumber=accNumber;
    }
    public static void acctName (String accName) {
        AccountDetails.accName=accName;
    }
    public static void     acctBalance (float accBalance) {
        AccountDetails.accBalance=accBalance;
    }
    public static long Deposite(int creditAmount) {
        AccountDetails.accBalance+=creditAmount;
        return AccountDetails.creNumber;
    }
    public static long Withdraw(int debitAmount) {
        AccountDetails.accBalance-=debitAmount;
        if(AccountDetails.accBalance<0) {
            AccountDetails.accBalance+=debitAmount;
        }
        return AccountDetails.creNumber;
    }
    public static long credNumber() {
        return AccountDetails.creNumber;
    }
}
