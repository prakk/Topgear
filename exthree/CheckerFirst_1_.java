package exthree;

import static org.junit.Assert.*;

import org.junit.Test;

public class CheckerFirst {

	@Test
	public void test() {
		AccountDetails.acctNo(1234564);
        AccountDetails.acctName("Employee One");
        AccountDetails.acctBalance(12.55f);
        //Depositing 20000
        assertEquals(AccountDetails.Deposite(20000),AccountDetails.creNumber);
        assertEquals(AccountDetails.accBalance,20012.55f,0.01);
        
        //withdrawing 100000 the answer will be negative so error
        assertEquals(AccountDetails.Withdraw(100000),AccountDetails.creNumber);
        assertEquals(AccountDetails.accBalance,-79987.453125f,0.01);
	}

}
