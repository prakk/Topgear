package exthree;

import static org.junit.Assert.*;

import org.junit.Test;

public class CheckerZeroth {

	@Test
	public void test() {
		AccountDetails.acctNo(1876564);
        AccountDetails.acctName("Employee Two");
        AccountDetails.acctBalance(1000000.55f);
        //Depositing 20000
        assertEquals(AccountDetails.Deposite(20000),AccountDetails.creNumber);
        assertEquals(AccountDetails.accBalance,1020000.5625f,0.01);
        
        //withdrawing 100000 the answer will be positive so no error
        assertEquals(AccountDetails.Withdraw(100000),AccountDetails.creNumber);
        assertEquals(AccountDetails.accBalance,920000.5625f,0.01);
	}

}
