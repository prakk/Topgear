package exthree;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)

@SuiteClasses({ CheckerFirst.class, CheckerZeroth.class })

public class AllTests {

}
