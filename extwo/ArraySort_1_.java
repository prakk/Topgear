package extwo;

public class ArraySort {
	public static int[] mergeSort(int[] arr, int low,int high) {
		int mid;
		if(low<high) {
			mid=(int)(low+high)/2;
			mergeSort(arr,low,mid);
			mergeSort(arr,mid+1,high);
			merger(arr,low,high,mid);
		}
		return arr;
	}
	public static void merger(int[] arr,int low,int high,int mid) {

		if(low<high) {

			int [] brr=new int[arr.length];
			int k=low,l=mid+1,j=low;
			while(k<=mid && l<=high) {
				if(arr[k]<arr[l])
					brr[j++]=arr[k++];
				else
					brr[j++]=arr[l++];
			}
			for(int i=0;i<low;i++)
				brr[i]=arr[i];
			while(j<arr.length && k<=mid)
				brr[j++]=arr[k++];
			while(j<arr.length && l<=high)
				brr[j++]=arr[l++];
			while(j<arr.length)
				brr[j++]=arr[j-1];
			
			for(int i=0;i<brr.length;i++)
				arr[i]=brr[i];
			
		}
	}
	public static void main(String[] args) {
		int[] arr= {9,6,3,4,-18};
		mergeSort(arr,0,arr.length-1);
		for(int i=0;i<arr.length;i++)
		System.out.print (arr[i]+" ");
	}
}

