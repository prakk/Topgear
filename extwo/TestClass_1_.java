package extwo;

import org.junit.Assert;
import org.junit.Test;

public class TestClass {
	
	@Test
	public void testOne() {
		int expResult[]= {1,2,3,45};
		Assert.assertArrayEquals(expResult,ArraySort.mergeSort(new int[] {1,2,3,45},0,3));//already sorted
	}
	
	@Test
	public void testTwo() {
		int expResult[]= {1,2,3,4,5};
		Assert.assertArrayEquals(expResult,ArraySort.mergeSort(new int[] {5,4,3,2,1},0,4));//sorted array result {1,2,3,4,5}
	}
	
	@Test
	public void testThree() {
		int expResult[]= {-1,1,2,3,7,9,45,69};
		Assert.assertArrayEquals(expResult,ArraySort.mergeSort(new int[] {1,2,3,45,9,-1,7,69},0,7));//345 not present
	}
	
	@Test
	public void testFour() {
		int expResult[]= {-18,1,2,3};
		Assert.assertArrayEquals(expResult,ArraySort.mergeSort(new int[] {1,2,3,-18},0,3));//sorted array result {-18,1,2,3}
	}
}