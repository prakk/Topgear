package exone;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;



public class TestClass {
	
	@Test
	public void testOne() {
		assertEquals(5,WordCount.count("Wipro123"));//words count is 5 integers count is 3
	}
	
	@Test
	public void testTwo() {
		assertEquals(5,WordCount.count("123Wipro123"));//words count is 5 integers count is 6
	}
	
	@Test
	public void testThree() {
		assertEquals(0,WordCount.count("123456"));//words count is 0 integers count is 6
	}
	
	@Test
	public void testFour() {
		assertEquals(0,WordCount.count(""));//words count is 0 integers count is 0
	}
}
