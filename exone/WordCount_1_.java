package exone;

import java.util.Scanner;


public class WordCount {
public static int count(String str) {
	int count=0;
	for(int i=0;i<str.length();i++)
		if(str.charAt(i)>=65 && str.charAt(i)<=90)
			count++;
		else if(str.charAt(i)>=97 && str.charAt(i)<=122)
			count++;
	return count;
}
public static void main(String[] args) {
	Scanner input=new Scanner(System.in);
	String inputString=input.nextLine();
	int count=count(inputString);
	System.out.println(count);
	input.close();
}
}
