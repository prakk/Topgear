package exfive;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class ParameterizedTest {

    private int price;
    private double discount;
    private double expected;
    private Book bookObject;
    
    @Before
    public void initialize() {
       bookObject = new Book();
    }
    
    public ParameterizedTest(int price, double discount, double expected) {
        this.price = price;
        this.discount = discount;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {10, 20, 8},
                {20, 10, 18},
                {30, 2, 29.4}
        });
    }

    @Test
    public void test_addTwoNumbes() {
        assertEquals( expected,bookObject.discountedPrice(price, discount)	,0.01);
    }

}