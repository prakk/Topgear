import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

class TestClass {
	String drivername;
	String url;
	String username;
	String pwd;
	
	@Before
	void setUpBeforeClass(String drivername,String url, String username, String pwd)
	{
		this.drivername=drivername;
		this.url=url;
		this.username=username;
		this.pwd=pwd;
	}
	@Test
	void DBConnectionTest() {
		DBConnection dbConnect=new DBConnection();
		this.setUpBeforeClass("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@localhost:1521:orcl","scott","tiger");
		assertArrayEquals("Success", dbConnect.Stringconnect(drivername, url, username, pwd).toCharArray(), "success".toCharArray());
		
	}

}
